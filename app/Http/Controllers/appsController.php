<?php

namespace App\Http\Controllers;

use App\Apps;
use App\Http\Requests\CreateAppRequest;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Image;

class appsController extends Controller
{
    /**
     * Return list of apps.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Apps::all();
    }

    /**
     * Create app.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAppRequest $request)
    {
        $data = $request->all();
        try {
            if (isset($data['logo'])) {
                $data['logo'] = $this->storeImage($data);
            }
            $response = Apps::create($data);
            return response()->json($response, 200);
        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'error' => $e->getCode(),
            ];
        }
    }

    /**
     * Create app.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeImage($data)
    {
        try {
            $fileName = '';
            $imageData = $data['logo'];
            $validB64 = preg_match(
                "/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).base64,.*/",
                $imageData);
            if ($validB64 && strlen($imageData) !== 0) {
                $fileName = Carbon::now()->timestamp
                    . '_'
                    . uniqid()
                    . '.'
                    . explode(
                        '/',
                        explode(':',
                            substr($imageData, 0, strpos($imageData, ';'))
                        )[1]
                    )[1];
                Image::make($data['logo'])->save(public_path('/images/') . $fileName);
                return $fileName;
            }
        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'error' => $e->getCode(),
            ];
        }
    }

    /**
     * Update app.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CreateAppRequest $request, $id)
    {
        $data = $request->all();
        try {
            if (isset($data['logo'])) {
                $data['logo'] = $this->storeImage($data);
            }
            if (Apps::where('id', $id)->update($data)) {
                $response = Apps::find($id);
                return response()->json($response, 200);
            }
        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'error' => $e->getCode(),
            ];
        }
    }

    /**
     * Delete app.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $response = Apps::destroy($id);
            return response()->json($response, 200);
        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'error' => $e->getCode(),
            ];
        }
    }
}
